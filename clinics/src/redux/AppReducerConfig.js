const AppReducerConfig = (prevState = { clinics: [] }, action) => {
    switch (action.type) {
        case "ADD": { //method for pushing all items into redux persistence memory storage
            let clinics = [...prevState.clinics];
            if (clinics.length < 112) { //ensuring all items will be pushed only once
                return { ...prevState, clinics: [...prevState.clinics, action.payload] }
            } else {
                return { ...prevState }
            }
        }
        case "UPDATE": { //method for updating clinic which stored in redux persistence memory storage
            let updateClinics = [...prevState.clinics]; //getting all items from storage
            let index = updateClinics.findIndex(clinic => { //getting clinic index by parameters
                return (clinic.X === action.payload.X) &&
                    (clinic.Y === action.payload.Y) && (clinic.ID === action.payload.ID)
            });
            updateClinics[index] = action.payload //updating clinic in index location
            return { ...prevState, clinics: updateClinics } //updating the store
        }
        default:
            return { ...prevState }
    }
}

export default AppReducerConfig;