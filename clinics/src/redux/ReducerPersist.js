import { createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import rootReducer from './AppReducerConfig.js'

const persistConfig = { //configurations for persist storage
    key: "root",
    storage
};

const persistedReducer = persistReducer(persistConfig, rootReducer) //creating persisted reducer by storage & store configs

export default () => {
    let store = createStore(persistedReducer) //creating the store
    let persistor = persistStore(store) //creating the persistor
    return { store, persistor }
}