import React, { Component } from 'react'
import { connect } from 'react-redux'

export class UpdateClinicComp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: this.props.clinic.Name, service: this.props.clinic.HMOname,
            address: this.props.clinic.address, neigh: this.props.clinic.neighborho
        }
    }

    updateClinic = () => {
        //Creating the clinic object to update by the properties that stored in the state 
        let newClinic = {
            X: this.props.clinic.X,
            Y: this.props.clinic.Y,
            Z: this.props.clinic.Z,
            ID: this.props.clinic.ID,
            Name: this.state.name,
            address: this.state.address,
            neighborho: this.state.neigh,
            HMOname: this.state.service
        }
        //Sending an update action to redux persistence memory storage
        let action = {
            type: "UPDATE",
            payload: newClinic
        }
        this.props.dispatch(action);
        //Announcing the user that update happend succefully and returning to main component
        alert('Clinic has been updated');
        this.props.callback(); //method that was passed as prop that changes the showUpdateComp in 'Main component'
    }

    render() {
        let clinic = this.props.clinic
        return (
            <div>
                <h1>Edit Health Clinic</h1>
                <strong>ID:</strong> {' '} {clinic.ID} <br />
                <strong>X:</strong> {' '} {clinic.X} <br />
                <strong>Y:</strong> {' '} {clinic.Y} <br />
                <strong>Z:</strong> {' '} {clinic.Z} <br />
                <strong>Name:</strong> {' '} <input type='text' onChange={(e) => this.setState({ name: e.target.value })}
                    defaultValue={clinic.Name} /> <br />
                <strong>Service Provider:</strong> {' '} <input type='text' onChange={(e) => this.setState({ service: e.target.value })}
                    defaultValue={clinic.HMOname} /> <br />
                <strong>Address:</strong> {' '} <input type='text' defaultValue={clinic.address}
                    onChange={(e) => this.setState({ address: e.target.value })} /> <br />
                <strong>Neighborhood:</strong> {' '} <input type='text' defaultValue={clinic.neighborho}
                    onChange={(e) => this.setState({ neigh: e.target.value })} /> <br /> <br />

                <input type='button' value='Update' onClick={() => this.updateClinic()} /> {' '}
                <input type='button' value='Cancel' onClick={() => { this.props.callback() }} />
            </div>
        )
    }
}

export default connect()(UpdateClinicComp)