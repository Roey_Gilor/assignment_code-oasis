import React, { Component } from 'react'
import { connect } from 'react-redux'

class AllClinicsComp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clinicsArr: [], sortParam: 'neighborho', order: 'ascending',
            beginArr: [], filter: '', filterParam: 'neighborho', filterValue: ''
        }
    }

    sortClinics = () => {
        let param = this.state.sortParam; //Getting the sort parameter
        let order = this.state.order //Getting the sorrt order
        let arr = this.state.clinicsArr; //Getting the clinics array
        if (param === 'neighborho') { //Sorting by neighborho parameter
            if (order === 'ascending') {
                //Sorting by Ascending order
                arr.sort((a, b) => (a.neighborho > b.neighborho) ? 1 : (a.neighborho < b.neighborho) ? -1 : 0)
            } else {
                //Descending by Ascending order
                arr.sort((a, b) => (a.neighborho < b.neighborho) ? 1 : (a.neighborho > b.neighborho) ? -1 : 0)
            }
        }
        else {
            if (param === 'HMOname') { //Sorting by service provider parameter
                if (order === 'ascending') {
                    //Sorting by Ascending order
                    arr.sort((a, b) => (a.HMOname > b.HMOname) ? 1 : (a.HMOname < b.HMOname) ? -1 : 0)
                } else {
                    //Descending by Ascending order
                    arr.sort((a, b) => (a.HMOname < b.HMOname) ? 1 : (a.HMOname > b.HMOname) ? -1 : 0)
                }
            }
        }
        this.setState({ clinicsArr: arr }) //setting the Array to be presented after being sorted
    }

    reset = () => {
        window.location.reload(); //refreshing the page in order to present the original Array before any change
    }

    componentDidMount = () => {
        //Removing duplicate items from array and setting the data to be presented
        const map = {};
        const newArray = [];
        this.props.data.clinics.forEach(el => {
            if (!map[JSON.stringify(el)]) {
                map[JSON.stringify(el)] = true;
                newArray.push(el);
            }
        });
        this.setState({ clinicsArr: newArray })
        this.setState({ beginArr: newArray })
    }

    updateClinic = (clinic) => {
        //method that was passed as prop that changes the showUpdateComp in 'Main component'
        this.props.callback(clinic)
    }

    getRow = (clinic, index) => {
        //return a row with all data & button that leads to update 'pgae'
        const btn = <input type='button' value='Edit' onClick={() => { this.updateClinic(clinic) }} />
        return <tr key={index}>
            <td>{clinic.ID}</td>
            <td>{clinic.X}</td>
            <td>{clinic.Y}</td>
            <td>{clinic.Z}</td>
            <td>{clinic.Name}</td>
            <td>{clinic.HMOname}</td>
            <td>{clinic.neighborho}</td>
            <td>{clinic.address}</td>
            <td>{btn}</td>
        </tr>
    }

    render() {
        let input = ''; //Input value to be pushed into filterValue in state
        let clinicsToRender; //Getting data from array to be inserted into table
        if (this.state.clinicsArr.length !== 0) {
            clinicsToRender = <table border='1'>
                {/* Creating table header */}
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>X</th>
                        <th>Y</th>
                        <th>Z</th>
                        <th>Name</th>
                        <th>Service Provider</th>
                        <th>Neighborhood</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {/* inserting data from array into table by parameter filter. if there is parameter value,
                inserting by value & type filter. otherwise, inserting all data into table */}
                    {this.state.filterValue === '' ? this.state.clinicsArr.map((clinic, index) => {
                        return this.getRow(clinic, index)
                    }) : this.state.clinicsArr.map((clinic, index) => {
                        if (this.state.filterParam === 'neighborho') {
                            if (clinic.neighborho.includes(this.state.filterValue)) {
                                return this.getRow(clinic, index)
                            }
                        } else if (this.state.filterParam === 'HMOname') {
                            if (clinic.HMOname.includes(this.state.filterValue)) {
                                return this.getRow(clinic, index)
                            }
                        }
                    })}
                </tbody>
            </table>
        }
        return (
            <div>
                <h1>Health Clinics</h1>
                {/* Setting the sorting parameter */}
                <label htmlFor="sortParam">Sort Clinics by: </label>
                <select id="sortParam" onChange={(e) => this.setState({ sortParam: e.target.value })}>
                    <option defaultValue value="neighborho">Neighborhood</option>
                    <option value="HMOname">Service Provider</option>
                </select> {' '}
                {/* Setting the sorting order */}
                <select id="order" onChange={(e) => this.setState({ order: e.target.value })}>
                    <option defaultValue value="ascending">Ascending</option>
                    <option value="descending">Descending</option>
                </select> {' '}
                <input type="button" value="Sort" onClick={() => this.sortClinics()} /> <br /> <br />

                {/* Setting the filtering parameter */}
                <label htmlFor="filterParam">Filter Clinics by: </label>
                <select id="filterParam" onChange={(e) => { this.setState({ filterParam: e.target.value }) }}>
                    <option defaultValue value="neighborho">Neighborhood</option>
                    <option value="HMOname">Service Provider</option>
                </select> {' '}
                {/* Setting the filtering value */}
                <input type='text' placeholder='Enter value' onChange={(e) => { input = e.target.value }} /> {' '}
                <input type="button" value="Filter" onClick={() => this.setState({ filterValue: input })} /> <br /> <br />

                {/* Reset button */}
                <input type='button' value='Reset' onClick={() => this.reset()} /> <br /> <br />

                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    {clinicsToRender}
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        data: state
    }
}

export default connect(mapStateToProps)(AllClinicsComp)