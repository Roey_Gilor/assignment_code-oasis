import React, { Component } from 'react'
import { connect } from 'react-redux'
import AllClinicsComp from './AllClinicsComp'
import UpdateClinicComp from './UpdateClinicComp'

export class MainComponent extends Component {
    constructor(props) {
        super(props)
        this.state = { showUpdateComp: false, clinic: '' }
    }

    componentDidMount = () => { //pushing all json data from file to redux persistence memory storage
        let clinics = require('../jsonAccess/healthclinics.json')
        clinics.map((clinic) => {
            let action = {
                type: "ADD",
                payload: clinic
            }
            this.props.dispatch(action);
        })
    }

    goToUpdateComponent = (clinic) => {
        //Setting the clinic data that will be send to the update component
        this.setState({ clinic: clinic })
        //Changing the showComp state that determine which component will be presented (update component)
        this.setState({ showUpdateComp: true })
    }

    goBackToMainComponent = () => {
        //Changing the showComp state that determine which component will be presented (All clinics component)
        this.setState({ showUpdateComp: false })
    }

    render() {
        return (
            <div>
                {/* Showing the wanted Component by showUpdateComp state */}
                {!this.state.showUpdateComp && <AllClinicsComp callback={(clinic) => this.goToUpdateComponent(clinic)} />}
                {this.state.showUpdateComp && <UpdateClinicComp clinic={this.state.clinic}
                    callback={() => this.goBackToMainComponent()} />}
            </div>
        )
    }
}

export default connect()(MainComponent)